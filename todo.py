#!/usr/bin/env python

from rich import print
from rich.pretty import pprint
from rich.console import Console
from rich.table import Table, box
from rich.prompt import Prompt, IntPrompt
import sys
import csv
import os
import time
import pyfiglet
from datetime import datetime
from csv import DictReader
from dateutil.parser import parse

console = Console()


def get_todos_by_project():
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        table = Table(box=box.ROUNDED)
        table.add_column("ID", justify="right", style="cyan")
        table.add_column(
            "Description", width=60, no_wrap=False, header_style="cyan", style="italic"
        )
        table.add_column("Project", header_style="cyan", width=12)
        table.add_column(
            "Due-Date",
            width=10,
            header_style="cyan",
            style="orange1",
            no_wrap=True,
            overflow="crop",
        )
        project = Prompt.ask(
            "\n\t >> Which [cyan b]'Project - tasks'[/cyan b] do you want to view? "
        ).upper()
        os.system("clear")
        data = csv.reader(f)
        found: bool = False
        for row in data:
            if row[5].strip() == str(project):
                found: bool = True
                ID = row[0]
                DESCRIPTION = row[4]
                PROJECT = row[5]
                DUE = row[7]

                table.add_row(f"{ID}", f"{DESCRIPTION}", f"{PROJECT}", f"{DUE}")
        f = pyfiglet.figlet_format("ToDo App", font="graceful")
        print("[red]{}[/red]".format(f))
        console.print(table)

        if found is False:
            print(f"\t >> No [b]Project[/b] exists of type [red b]{project}[/red b]")
            time.sleep(3)


def get_todos_by_context():
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        table = Table(box=box.ROUNDED)
        table.add_column("ID", justify="right", style="cyan")
        table.add_column(
            "Description", width=60, no_wrap=False, header_style="cyan", style="italic"
        )
        table.add_column("@Context", header_style="cyan", width=12)
        table.add_column(
            "Due-Date",
            width=10,
            header_style="cyan",
            style="orange1",
            no_wrap=True,
            overflow="crop",
        )
        context = (
            "@"
            + Prompt.ask(
                "\n\t >> Which [cyan b]'Context - tasks'[/cyan b] do you want to view?",
            ).lower()
        )  # noqa: E501
        os.system("clear")
        data = csv.reader(f)
        found: bool = False
        for row in data:
            if row[6].strip() == str(context):
                found: bool = True
                ID = row[0]
                DESCRIPTION = row[4]
                CONTEXT = row[6]
                DUE = row[7]

                table.add_row(f"{ID}", f"{DESCRIPTION}", f"{CONTEXT}", f"{DUE}")
        f = pyfiglet.figlet_format("ToDo App", font="graceful")
        print("[red]{}[/red]".format(f))
        console.print(table)

        if found is False:
            print(f"\t >> No [b]@Context[/b] exists of type [red b]{context}[/red b]")
            time.sleep(3)


def get_todos_by_priority():
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        table = Table(box=box.ROUNDED)
        table.add_column("ID", justify="right", style="cyan")
        table.add_column("P", justify="center", header_style="cyan", style="red1")
        table.add_column(
            "Description", width=60, no_wrap=False, header_style="cyan", style="italic"
        )
        table.add_column(
            "Due-Date",
            width=10,
            header_style="cyan",
            style="orange1",
            no_wrap=True,
            overflow="crop",
        )
        priority = Prompt.ask(
            "\n\t >> Which [cyan b]'Priority - tasks'[/cyan b] do you want to view? "
        ).upper()
        os.system("clear")
        data = csv.reader(f)
        found: bool = False
        for row in data:
            if row[2].strip() == str(priority):
                found: bool = True
                ID = row[0]
                PRIORITY = row[2]
                DESCRIPTION = row[4]
                DUE = row[7]
                table.add_row(f"{ID}", f"{PRIORITY}", f"{DESCRIPTION}", f"{DUE}")
        f = pyfiglet.figlet_format("ToDo App", font="graceful")
        print("[red]{}[/red]".format(f))
        console.print(table)

        if found is False:
            print(f"\t >> No [b]Priority[/b] exists of type [red b]{priority}[/red b]")
            time.sleep(3)


def add_todo():
    now: datetime = datetime.now()
    csv_file = "todos.csv"
    with open(csv_file, "r", newline="", encoding="utf-8") as f:
        data = DictReader(f)
        id = []
        for col in data:
            id.append(col["ID"])
        last_id = int(id[-1])

    next_id = str(last_id + 1)
    print(f"Next ID: {next_id}")
    status = Prompt.ask(
        "Enter a 'Status' (/, x, . or leave empty): ",
        choices=["/", "x", ".", ""],
        show_choices=False,
    )
    priority = Prompt.ask(
        "Enter a 'Priority' (A, B, C or leave empty): ",
        choices=["A", "a", "B", "b", "C", "c", ""],
        show_choices=False,
    ).upper()
    create_on = f"{now:%d-%m-%Y}"
    while True:
        desc = Prompt.ask("Enter a Description")
        if len(desc) <= 60:
            break
        else:
            print("[red r]Max: 60 characters allowed[/red r]")
    while True:
        project = Prompt.ask("Enter 'Project' (title or leave empty): ").upper()
        if len(project) <= 12:
            break
        else:
            print("[red r]Max: 12 characters allowed[/red r]")
    while True:
        context = "@" + Prompt.ask("Enter '@Context' or leave empty: ").lower()
        if len(context) <= 12:
            break
        else:
            print("[red r]Max: 12 characters allowed[/red r]")
    due_date = parse(Prompt.ask("Enter a DueDate (dd-mm-YYYY) or leave empty: "))  # noqa: E501

    row = [
        next_id,
        status,
        priority,
        create_on,
        desc,
        project,
        context,
        due_date,
    ]  # noqa: E501
    try:
        with open(csv_file, "a", newline="", encoding="utf-8") as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(row)
    except FileNotFoundError:
        print(f"File {csv_file} not found.!")
    except csv.Error as e:
        print(f" An error occurred while appending to {csv_file}: {e}")
    os.system("clear")

    f = pyfiglet.figlet_format("ToDo App", font="graceful")
    print("[red]{}[/red]".format(f))
    view_all_todos()


def delete_todo():
    task = []
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        delete_row = IntPrompt.ask("\t >> Which 'ToDo' do you want to delete?")
        data = csv.reader(f)
        found: bool = False
        for row in data:
            if row[0] == str(delete_row):
                found: bool = True
                print(f"\t[green1 r] >> ToDo [b]#{delete_row}[/b] Deleted![/green1 r]")
                time.sleep(2)
            else:
                task.append(row)
        if found is False:
            print("\t[red r] >> ToDo not found![/red r]")
            time.sleep(2)
        else:
            with open("todos.csv", "w+", newline="") as updated:
                deleted = csv.writer(updated)
                deleted.writerows(task)
                updated.seek(0)
                data = csv.reader(updated)
                view_all_todos()


def status_todo():
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        edit_row = IntPrompt.ask("\t >> Which ToDo 'Status' do you want to edit?")
        data = csv.reader(f)
        completed = []
        found: bool = False
        for row in data:
            if row[0] == str(edit_row):
                found: bool = True
                status: list[str] = ["x", "/", ".", ""]
                row[1] = Prompt.ask(
                    "\t >> Change Status: (/, x, . or leave empty)",
                    choices=["/", "x", ".", ""],
                    show_choices=False,
                ).lower()
                while row[1] not in status:
                    print("\n\t[red r] >> Only x, /, . or 'empty' accepted![/red r]")
                    row[1] = Prompt.ask(
                        "\t >> Change Status: (/, x, . or leave empty)",
                        choices=["/", "x", ".", ""],
                        show_choices=False,
                    ).lower()
                else:
                    print("\t[green1 r] >> ToDo Status updated![/green1 r]", end="")
                    pprint(row)
                    time.sleep(2)
            completed.append(row)
        if found is False:
            print("\t[red r] >> ToDo Not found cannot update Status [/red r]")
            time.sleep(2)
        else:
            with open("todos.csv", "w", newline="", encoding="utf-8") as f:
                update = csv.writer(f)
                update.writerows(completed)
                view_all_todos()


def priority_todo():
    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        edit_row = IntPrompt.ask("\t >> Which ToDo 'Priority' do you want to edit?")
        data = csv.reader(f)
        completed = []
        found: bool = False
        for row in data:
            if row[0] == str(edit_row):
                found: bool = True
                priorities: list[str] = ["A", "B", "C", ""]
                row[2] = Prompt.ask(
                    "\t >> Change Priority (A, B, C or leave empty)",
                    choices=["A", "a", "B", "b", "C", "c", ""],
                    show_choices=False,
                ).upper()
                while row[2] not in priorities:
                    print("\n\t[red r] >> Only A, B, C or 'empty' accepted![/red r]")
                    row[2] = Prompt.ask(
                        "\t >> Change Priority (A, B, C or leave empty)",
                        choices=["A", "a", "B", "b", "C", "c", ""],
                        show_choices=False,
                    ).upper()
                else:
                    print("\t[green1 r] >> Updated Row: [/green1 r]", end="")
                    pprint(row)
                    time.sleep(2)
            completed.append(row)
        if found is False:
            print("\t[red r] >> ToDo Not Found cannot update Priority[/red r] ")
            time.sleep(2)
        else:
            with open("todos.csv", "w", newline="", encoding="utf-8") as f:
                update = csv.writer(f)
                update.writerows(completed)
                view_all_todos()


def view_all_todos():
    os.system("clear")
    f = pyfiglet.figlet_format("ToDo App", font="graceful")
    print("[red]{}[/red]".format(f))

    print(
        """
        \n[b]Status:[/b] [gold1 b]x[/gold1 b] - completed, [gold1 b]/[/gold1 b] - partial, [gold1 b].[/gold1 b] - paused. [b]Priority:[/b] [red1]A, B or C[/red1]
        """
    )
    table = Table(box=box.ROUNDED)
    table.add_column("ID", justify="right", style="cyan")
    table.add_column("S", justify="center", header_style="cyan", style="gold1")
    table.add_column("P", justify="center", header_style="cyan", style="red1")
    table.add_column("Created-On", width=10, header_style="cyan", style="green4")
    table.add_column(
        "Description", width=60, no_wrap=False, header_style="cyan", style="italic"
    )
    table.add_column("Project", header_style="cyan", width=12)
    table.add_column("Context", header_style="cyan", width=12)
    table.add_column(
        "Due-Date",
        width=10,
        header_style="cyan",
        style="orange1",
        no_wrap=True,
        overflow="crop",
    )

    with open("todos.csv", "r", newline="", encoding="utf-8") as f:
        data = DictReader(f)
        for row in data:
            values = list(row.values())
            ID = values[0]
            COMPLETED = values[1]
            PRIORITY = values[2]
            CREATED = values[3]
            DESCRIPTION = values[4]
            PROJECT = values[5]
            CONTEXT = values[6]
            DUE = values[7]

            table.add_row(
                f"{ID}",
                f"{COMPLETED}",
                f"{PRIORITY}",
                f"{CREATED}",
                f"{DESCRIPTION}",
                f"{PROJECT}",
                f"{CONTEXT}",
                f"{DUE}",
            )

    console = Console()
    console.print(table)


def close():
    exit()


# Main Menu
menu = {
    1: "Add ToDo",
    2: "Delete ToDo",
    3: "Change Status",
    4: "Change Priority",
    5: "View by Project",
    6: "View by @Context",
    7: "View by Priority",
    8: "View All",
    0: "Quit",
}


def main():
    print("\n\t[cyan1] Main Menu[/cyan1]")
    print("\t", "-" * 20)
    for key in menu.keys():
        print("\t", key, "--", menu[key])


if __name__ == "__main__":
    f = pyfiglet.figlet_format("ToDo App", font="graceful")
    print("[red]{}[/red]".format(f))
    while True:
        main()
        user_input = ""
        try:
            user_input = IntPrompt.ask("\n\t >> Choose an option from the menu")
        except Exception:
            print("\n\t[red r] >> Wrong input. Please enter a number...[/red r]")
        if user_input == 1:
            add_todo()
        elif user_input == 2:
            delete_todo()
        elif user_input == 3:
            status_todo()
        elif user_input == 4:
            priority_todo()
        elif user_input == 5:
            get_todos_by_project()
        elif user_input == 6:
            get_todos_by_context()
        elif user_input == 7:
            get_todos_by_priority()
        elif user_input == 8:
            view_all_todos()
        elif user_input == 0:
            print("\t[b yellow1] >> Thank you, Goodbye.[/yellow1 b]\n")
            time.sleep(2)
            os.system("clear")
            sys.exit()
        else:
            print("\t[red r] >> Invalid entry, try again.![/red r]")
