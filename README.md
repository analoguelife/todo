# todo
## ToDo application for Terminal written in Python

Current **ToDo's** loaded on programme start.<br>
Possible to list out **ToDo's** by *Project*, *@context* and *Priority*<br>

       ____  __  ____   __      __   ____  ____
      (_  _)/  \(    \ /  \    / _\ (  _ \(  _ \
        )( (  O )) D ((  O )  /    \ ) __/ ) __/
       (__) \__/(____/ \__/   \_/\_/(__)  (__)

 		### ToDo's: Status: x - completed, / - partial,. - paused. Priority: A, B or C ###
		╭────┬────┬────┬────────────┬──────────────────────────────────────────────────────────────┬──────────────┬──────────────┬────────────╮
		│ ID │ S  │ P  │ Created-On │ Description                                                  │ Project      │ Context      │ Due-Date   │
		├────┼────┼────┼────────────┼──────────────────────────────────────────────────────────────┼──────────────┼──────────────┼────────────┤
		│ 3  │ x  │ A  │ 2024-02-09 │ Complete application for Hunters Yard post                   │ Personal     │ @jobhunt     │ 2024-02-15 │
		│    │    │    │            │                                                              │              │              │            │
		│ 4  │ .  │    │ 2024-02-09 │ Renew MGOC membership                                        │ Car          │ @mgbGT       │ 2024-02-28 │
		│    │    │    │            │                                                              │              │              │            │
		│ 5  │ /  │ A  │ 2024-02-09 │ Add Python rich & Python typer functionality to ToDo App     │ Python       │ @coding      │ 2024-03-30 │
		│    │    │    │            │                                                              │              │              │            │
		│ 6  │    │ C  │ 2024-02-09 │ Begin looking at developing PhotoDev App                     │ Python       │ @coding      │ 2024-06-30 │
		╰────┴────┴────┴────────────┴──────────────────────────────────────────────────────────────┴──────────────┴──────────────┴────────────╯

Terminal layout conforms to [todo.txt](https://github.com/todotxt/todo.txt) guidelines.
### Status conditions
* empty
* completed - x
* partial - /
* started - .

### Priority conditions
* empty
* **A**, **B** or **C**

### CreatedOn & DueDate
* in format - dd-mm-YYYY

### Description
* Anything you want to a *max* 60 chars.

### Project & @Context
* empty
* associated info to a *max* 12 chars.

## Menu as is stands:
	 ---------------------
	 Main Menu - ToDo App.
	 ---------------------
    1: "Add ToDo",
    2: "Delete ToDo",
    3: "Change Status",
    4: "Change Priority",
    5: "View by Project",
    6: "View by @Context",
    7: "View by Priority",
    8: "View All",
    0: "Quit",

	 >> Choose an option from the menu:

## See *requirements.txt* for python modules
### Uses:
* rich - for colors and formatting in terminal
* pyfiglet - for ASCII art
* dateutil - checking format of string dates

## ToDo's


